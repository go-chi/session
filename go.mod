module gitea.com/go-chi/session

go 1.21

require (
	github.com/bradfitz/gomemcache v0.0.0-20190329173943-551aad21a668
	github.com/couchbase/go-couchbase v0.0.0-20201026062457-7b3be89bbd89
	github.com/go-chi/chi/v5 v5.0.4
	github.com/go-redis/redis/v8 v8.4.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/lib/pq v1.2.0
	github.com/siddontang/ledisdb v0.0.0-20190202134119-8ceb77e66a92
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/unknwon/com v1.0.1
	gopkg.in/ini.v1 v1.62.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/couchbase/gomemcached v0.1.1 // indirect
	github.com/couchbase/goutils v0.0.0-20201030094643-5e82bb967e67 // indirect
	github.com/cupcake/rdb v0.0.0-20161107195141-43ba34106c76 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/siddontang/go v0.0.0-20180604090527-bdc77568d726 // indirect
	github.com/siddontang/rdb v0.0.0-20150307021120-fc89ed2e418d // indirect
	github.com/smartystreets/assertions v0.0.0-20190116191733-b6c0e53d7304 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	go.opentelemetry.io/otel v0.14.0 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/net v0.0.0-20201006153459-a7d1128ccaa0 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	google.golang.org/appengine v1.6.7 // indirect
)
